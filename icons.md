# Icons
We use [Bootstrap Glyphicons][1], [Material Icons][2], and [Font Awesome][3] in our apps. Since Bootstrap is the dominant framework used in the SBM, prefer using Bootstrap glyphicons where possible.

## Preferred Icons
* Deleting / Removing items from a list
  * If removing an item from a list that can be easily added back, use an 'x' (glyphicon glyphicon-remove) or a minus (glyphicon glyphicon-minus)
  * If the deleting is permanent, use a trash can icon (glyphicon glyphicon-trash) and a confirmation dialog. The trash icon makes the is more alarming than the icon and 'x', since these are used for minimizing and closing a window on Windows. The confirmation dialog will help to prevent deleting if the user isn't sure what the icon is for and any other accidental data loss.
* Editing (glyphicon glyphicon-penci)
* Collapsing (glyphicon glyphicon-chevron-*)
* Sorting (glyphicon glyphicon-sort-*)


[1]: https://getbootstrap.com/docs/3.3/components/
[2]: https://material.angularjs.org/latest/api/directive/mdIcon
[3]: https://github.com/bokmann/font-awesome-rails
