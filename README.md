### Angular Style Guide ###

* https://github.com/johnpapa/angular-styleguide/tree/master/a1

### Ruby Style Guide ###

* https://github.com/rubocop-hq/ruby-style-guide

### Rails Style Guide ###

* https://github.com/rubocop-hq/rails-style-guide
