# Color scheme
We use the [Bootswatch Flatly theme][1] in the SBM and AP apps. The Sass variables used by Flatly can be found [here][2].


## Main Colors
[Visual color pallete][3]

![Bootswatch Flatly Main Colors][Bootswatch Flatly Main Colors]

* $brand-primary: #2C3E50 ![Brand Primary][Brand Primary]
* $brand-success: #18BC9C ![Brand Success][Brand Success]
* $brand-info:    #3498DB ![Brand Info][Brand Info]
* $brand-warning: #F39C12 ![Brand Warning][Brand Warning]
* $brand-danger:  #E74C3C ![Brand Danger][Brand Danger]

[1]: https://bootswatch.com/flatly/
[2]: https://github.com/maxim/bootswatch-rails/blob/master/vendor/assets/stylesheets/bootswatch/flatly/_variables.scss
[3]: https://coolors.co/2c3e50-18bc9c-3498db-f39c12-e74c3c
[Bootswatch Flatly Main Colors]: https://bitbucket.org/standardind/standard-web-apps-code-guide/downloads/color-scheme-main-colors-small.png
[Brand Primary]: https://bitbucket.org/standardind/standard-web-apps-code-guide/downloads/color-scheme-brand-primary.png
[Brand Success]: https://bitbucket.org/standardind/standard-web-apps-code-guide/downloads/color-scheme-brand-success.png
[Brand Info]: https://bitbucket.org/standardind/standard-web-apps-code-guide/downloads/color-scheme-brand-info.png
[Brand Warning]: https://bitbucket.org/standardind/standard-web-apps-code-guide/downloads/color-scheme-brand-warning.png
[Brand Danger]: https://bitbucket.org/standardind/standard-web-apps-code-guide/downloads/color-scheme-brand-danger.png